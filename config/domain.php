<?php

return [
  'env_stub' => '.env',
  'storage_dirs' => [
    'app' => [
      'public' => [
      ],
    ],
    'framework' => [
      'cache' => [
      ],
      'testing' => [
      ],
      'sessions' => [
      ],
      'views' => [
      ],
    ],
    'logs' => [
    ],
  ],
  'domains' => [
    'acasza.arianto.com' => 'acasza.arianto.com',
    'naia.arianto.com' => 'naia.arianto.com',
  ],
 ];