## Learning Laravel

1. Install laravel terbaru
2. Install laravel breeze
   - https://laravel.com/docs/8.x/starter-kits#laravel-breeze
3. Install library https://github.com/gecche/laravel-multidomain
   - Langkah pertama ganti dengan ini

```
$domainParams = [
    'domain_detection_function_web' => function() {
        return \Illuminate\Support\Arr::get($_SERVER,'HTTP_HOST');
    }
];

//$app = new Illuminate\Foundation\Application(
$app = new Gecche\Multidomain\Foundation\Application(
    $_ENV['APP_BASE_PATH'] ?? dirname(__DIR__), null, $domainParams
);
```

